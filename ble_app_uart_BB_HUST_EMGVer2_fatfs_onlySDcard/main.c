/**
 * Copyright (c) 2014 - 2020, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
 *
 * @defgroup ble_sdk_uart_over_ble_main main.c
 * @{
 * @ingroup  ble_sdk_app_nus_eval
 * @brief    UART over BLE application main file.
 *
 * This file contains the source code for a sample application that uses the Nordic UART service.
 * This application uses the @ref srvlib_conn_params module.
 */


#include <stdint.h>
#include <string.h>
#include <stdio.h>
//#include "nordic_common.h"


#include "nrf.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
//#include "softdevice_handler.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "app_timer.h"
#include "ble_nus.h"
#include "app_uart.h"
#include "app_util_platform.h"
#include "bsp_btn_ble.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_delay.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
// for test
#define FILE_NAME   "DU8.TXT"
#define TEST_STRING "SD card example.\r\n"
// for SD card
#include "diskio_blkdev.h"
#include "ff.h"
#include "nrf_block_dev_sdc.h"
#include "SEGGER_RTT.h"

// phan bien do minh viet

// 30000 phan tu cho data va 19 phan tu cho timestamp 1 phan tu cuoi cho ki tu \0. 
char EMG_data[30001];
char ADC_char[6];
uint32_t count_sample = 0;

uint16_t count_EMG_data=0;
char* user_information;
// update timestamp flag

uint16_t ADC_value;
bool user_info_written=false;

// for SD card

#define SDC_SCK_PIN     26  ///< SDC serial clock (SCK) pin.
#define SDC_MOSI_PIN    11  ///< SDC serial data in (DI) pin.
#define SDC_MISO_PIN    19  ///< SDC serial data out (DO) pin.
#define SDC_CS_PIN      28  ///< SDC chip select (CS) pin.

/**
 * @brief  SDC block device definition
 * */
NRF_BLOCK_DEV_SDC_DEFINE(
        m_block_dev_sdc,
        NRF_BLOCK_DEV_SDC_CONFIG(
                SDC_SECTOR_SIZE,
                APP_SDCARD_CONFIG(SDC_MOSI_PIN, SDC_MISO_PIN, SDC_SCK_PIN, SDC_CS_PIN)
         ),
         NFR_BLOCK_DEV_INFO_CONFIG("Nordic", "SDC", "1.00")
);


static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


static uint32_t data_buffer;


// phan nay bo sung cho SAADC
// lam viec o che do scan mode 

#define SAADC_SAMPLES_IN_BUFFER         2// SAMPLES_IN_BUFFER is 6 you will get: ch0, ch1, ch2, ch0, ch1, ch2. so SAADC_SAMPLES_IN_BUFFER dat bang voi so kenh ADC.

void saadc_init(void)
{
		NRF_SAADC->RESOLUTION = 2; // 12 BIT MODE
		NRF_SAADC->OVERSAMPLE = 0; //bypass oversample
		
		
		NRF_SAADC->CH[0].CONFIG = 0x0 << 0UL | 0x0 << 4UL | 0x0 << 8UL | 0x0 << 12UL | 0x2 << 16UL;
		NRF_SAADC->CH[0].PSELP = 1; // NRF_SAADC_INPUT_AIN0
		NRF_SAADC->CH[0].PSELN = 0; // NRF_SAADC_INPUT_DISABLED
		
		NRF_SAADC->INTEN = 0x00000;

		
		NRF_SAADC->RESULT.PTR = (uint32_t)(&data_buffer);
		NRF_SAADC->RESULT.MAXCNT = 1; // number of samples
}

void saadc_start()
{
		NRF_SAADC->ENABLE = 1;
    //start task;
    NRF_SAADC->TASKS_START = 0x01UL;
    while (!NRF_SAADC->EVENTS_STARTED){} 
		NRF_SAADC->EVENTS_STARTED = 0;
    NRF_SAADC->TASKS_SAMPLE = 1;
}
// ham ngat timer
void TIMER2_IRQHandler(void)
{
if ((NRF_TIMER2->EVENTS_COMPARE[0] != 0) && ((NRF_TIMER2->INTENSET & TIMER_INTENSET_COMPARE0_Msk) != 0))
  {

		NRF_TIMER2->EVENTS_COMPARE[0] = 0;           //Clear compare register 0 event		
		saadc_start();
		while(!NRF_SAADC->EVENTS_END){}
		NRF_SAADC->EVENTS_END = 0;
 }
}	 
//ham lay mau ADC

void adc_sampling()
{
		if(((uint16_t)data_buffer)>>15)
		{
			data_buffer=data_buffer & 0xffff0000;
		}
		char ADC_char[6];//khai bap mang ADC_char c� 6 phan tu
		uint16_t ADC_value;//khai bao ADC_vlue int 16 bit
	  		//lay gia tri ADC thu duoc
		ADC_value=(uint8_t)(data_buffer&0x3f)+(uint8_t)((data_buffer>>6UL)&0x3f)*64;
		NRF_LOG_INFO("ADC Value: %d", ADC_value);
			//chuyen thanh kieu ki tu
    sprintf(ADC_char,"%d\n",ADC_value);
		for(int i=count_EMG_data;i<count_EMG_data + strlen(ADC_char);i++)
			{
				EMG_data[i]=ADC_char[i-count_EMG_data];
				
			}
			// increase buffer with size ADC_char
		count_EMG_data=count_EMG_data + strlen(ADC_char);
}

void timer_init()
{
	NRF_TIMER2->MODE = TIMER_MODE_MODE_Timer;  // Set the timer in Counter Mode // 0x4000A000
  NRF_TIMER2->TASKS_CLEAR = 1;               // clear the task first to be usable for later
	NRF_TIMER2->PRESCALER = 6;                             //Set prescaler. Higher number gives slower timer. Prescaler = 0 gives 16MHz timer
	NRF_TIMER2->BITMODE = TIMER_BITMODE_BITMODE_08Bit;		 //Set counter to 16 bit resolution
	NRF_TIMER2->CC[0] = 10;                                //Set value for TIMER2 compare register 0

	NRF_TIMER2->INTENSET = (TIMER_INTENSET_COMPARE0_Enabled << TIMER_INTENSET_COMPARE0_Pos);
  NVIC_SetPriority(TIMER2_IRQn, 6);
	NVIC_EnableIRQ(TIMER2_IRQn);
	NRF_TIMER2->TASKS_START = 1;  	          // Start TIMER2 	          // Start TIMER2
}

//FATFS
static FATFS fs;  // file system object structure
static DIR dir;   // 
static FILINFO fno;// file information structure
static FIL file;   // file object structure

uint32_t bytes_written;
FRESULT  ff_result;
DSTATUS  disk_state = STA_NOINIT;

static void fatfs_open()
{
    // Initialize FATFS disk I/O interface by providing the block device.
	  //  
    static diskio_blkdev_t drives[] =
    {
            DISKIO_BLOCKDEV_CONFIG(NRF_BLOCKDEV_BASE_ADDR(m_block_dev_sdc, block_dev), NULL)
    };

    diskio_blockdev_register(drives, ARRAY_SIZE(drives));

    NRF_LOG_INFO("Initializing disk 0 (SDC)...\r\n");
    for (uint32_t retries = 3; retries && disk_state; --retries)
    {
			
        NRF_LOG_INFO(".");
        disk_state = disk_initialize(0);

    }
    if (disk_state)
    {
        NRF_LOG_INFO("Disk initialization failed.\r\n");
        return;
    }

    uint32_t blocks_per_mb = (1024uL * 1024uL) / m_block_dev_sdc.block_dev.p_ops->geometry(&m_block_dev_sdc.block_dev)->blk_size;
    uint32_t capacity = m_block_dev_sdc.block_dev.p_ops->geometry(&m_block_dev_sdc.block_dev)->blk_count / blocks_per_mb;
    NRF_LOG_INFO("Capacity: %d MB\r\n", capacity);

    NRF_LOG_INFO("Mounting volume...\r\n");
    ff_result = f_mount(&fs,"", 1);// mount is a process which the processor allow user can access file and folder.
    if (ff_result)
    {
        NRF_LOG_INFO("Mount failed.\r\n");
        return;
    }

    NRF_LOG_INFO("\r\n Listing directory: /\r\n");
    ff_result = f_opendir(&dir, "/");
    if (ff_result)
    {
        NRF_LOG_INFO("Directory listing failed!\r\n");
        return;
    }    
    do
    {
        ff_result = f_readdir(&dir, &fno); // hai thong so nhap vao la pointer thu muc dich, va pointer thong tin file 
        if (ff_result != FR_OK)
        {
            NRF_LOG_INFO("Directory read failed.");
            return;
        }
        if (fno.fname[0])
        {
            if (fno.fattrib & AM_DIR)// neu day la file cho truy cap truc tiep
            {
                NRF_LOG_RAW_INFO("   <DIR>   %s\r\n",(uint32_t)fno.fname);
            }
            else
            {
                NRF_LOG_RAW_INFO("%91u  %s\r\n", fno.fsize, (uint32_t)fno.fname);
            }
        }
    }
    while (fno.fname[0]);// neu ten file co ton tai thi lam dieu tren 
    NRF_LOG_RAW_INFO("\r\n");

    NRF_LOG_INFO("Writing to file " FILE_NAME "...\r\n");
  
    ff_result = f_open(&file,FILE_NAME, FA_READ | FA_WRITE | FA_OPEN_APPEND);
		
    if (ff_result != FR_OK)
    {
        NRF_LOG_INFO("Unable to open or create file: " FILE_NAME ".\r\n");
        return;
    }
		NRF_LOG_INFO("Successfull open file : " FILE_NAME ".");
}

static void fatfs_write()
{
    ff_result = f_open(&file,FILE_NAME, FA_READ | FA_WRITE | FA_OPEN_APPEND);
		
    if (ff_result != FR_OK)
    {
        NRF_LOG_INFO("Unable to open or create file: " FILE_NAME ".\r\n");
        return;
    }
//		if(user_info_written)
//		{		
//				ff_result = f_write(&file, EMG_data, strlen(EMG_data), (UINT *) &bytes_written);// this is the function write string into file
//				
//				if (ff_result != FR_OK)
//				{
//						NRF_LOG_INFO("Write failed\r\n.");
//				}
//				else
//				{
//						NRF_LOG_INFO("%d bytes written.\r\n", bytes_written);
//				}
//		}
		ff_result = f_write(&file, EMG_data, strlen(EMG_data), (UINT *) &bytes_written);// this is the function write string into file
    
		if (ff_result != FR_OK)
    {
        NRF_LOG_INFO("Write failed\r\n.");
    }
    else
    {
        NRF_LOG_INFO("%d bytes written.\r\n", bytes_written);
    }
    (void) f_close(&file);
		NRF_LOG_INFO("close file thanh cong");
    return;
}
	
int main(void)
{
  APP_ERROR_CHECK(NRF_LOG_INIT(NULL));
  NRF_LOG_INFO("\r\nFATFS example.\r\n\r\n");
  nrf_gpio_cfg_output(29);

  // Initialize.
  saadc_init();
  log_init();
  timer_init();
  fatfs_open();
  // Enter main loop.
  while (true) {
    if (disk_state) {
      f_close(&file);
      break;
    }
     else {
      if (count_sample == 2000) {
        fatfs_write();
        break;
      } 
      else {
        adc_sampling();
        count_sample++;
      }
    }
  }
}


/**
 * @}
 */